[TOC]

-----

# Press

## Notes:

The Daily News issued multiple super-hero-esque pieces on Platano Man, including a front-page spread. *Ebony Magazine* published good reporting of the incident, some of which was quoted by *Brietbart*.  *Gothamist* has the most up-to-date reporting, including news that the attacker may have committed earlier assaults that day.  *Brietbart* published its own article, somewhat biased toward the attacker through unwarranted use of the word "allegedly," as well as having no mention the victim's or Ayala's comments about race or Donald Trump.  However, *Brietbart* had the only mention of Ayala himself receiving injuries during the attack.  *AP* has done no reporting.